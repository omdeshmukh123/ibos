-- public.ibos_account_balance definition

-- Drop table

-- DROP TABLE public.ibos_account_balance;

CREATE TABLE public.ibos_account_balance (
	bank_code varchar(5) NULL,
	account_iban varchar(50) NULL,
	ibos_acc_id varchar(50) NULL,
	ibos_acc_date timestamptz NULL,
	acc_reference_no varchar(50) NULL,
	statement_seq_number varchar(50) NULL,
	open_bal_mark varchar(50) NULL,
	open_bal_currency varchar(5) NULL,
	open_bal_date varchar(50) NULL,
	open_bal_amount numeric NULL,
	close_bal_mark varchar(50) NULL,
	close_bal_currency varchar(5) NULL,
	close_bal_date varchar(50) NULL,
	close_bal_amount numeric NULL,
	close_avl_bal_mark varchar(50) NULL,
	close_avl_bal_currency varchar(5) NULL,
	close_avl_bal_date varchar(50) NULL,
	close_avl_bal_amount numeric NULL,
	fwd_avl_bal_mark varchar(50) NULL,
	fwd_avl_bal_currency varchar(5) NULL,
	fwd_avl_bal_date varchar(50) NULL,
	fwd_avl_bal_amount numeric NULL,
	narrative varchar(400) NULL
);


-- public.ibos_account_txn definition

-- Drop table

-- DROP TABLE public.ibos_account_txn;

CREATE TABLE public.ibos_account_txn (
	account_iban varchar(50) NULL,
	ibos_txn_id varchar(50) NULL,
	parent_ibos_acc_id varchar(50) NULL,
	txn_reference_no varchar(50) NULL,
	statement_seq_number varchar(50) NULL,
	floor_limit_mark varchar(50) NULL,
	floor_limit_currency varchar(50) NULL,
	floor_limit_amount numeric NULL,
	ibos_txn_date timestamptz NULL,
	value_date_string varchar(50) NULL,
	entry_date_string varchar(50) NULL,
	account_txn_mark varchar(50) NULL,
	funds_code varchar(50) NULL,
	account_txn_amount numeric NULL,
	account_txn_identifier_code varchar(50) NULL,
	ref_acc_owner varchar(50) NULL,
	ref_acc_serv_insttution varchar(50) NULL,
	acc_txn_supp_dtls varchar(50) NULL,
	narrative varchar(400) NULL
);


-- public.i_test definition

-- Drop table

-- DROP TABLE public.i_test;

CREATE TABLE public.i_test (
	orgnl_payment_id serial NOT NULL DEFAULT nextval('public.i_test_orgnl_payment_id_seq'::regclass),
	CONSTRAINT i_test_pkey PRIMARY KEY (orgnl_payment_id)
);

-- public.ibos_audit_log definition

-- Drop table

-- DROP TABLE public.ibos_audit_log;

CREATE TABLE public.ibos_audit_log (
	ibos_audit_id varchar(50) NULL,
	file_name varchar(200) NULL,
	incoming_timestamp timestamptz NULL,
	file_type varchar(50) NULL,
	file_status varchar(50) NULL,
	direction varchar(50) NULL
);


-- public.ibos_config_param_table definition

-- Drop table

-- DROP TABLE public.ibos_config_param_table;

CREATE TABLE public.ibos_config_param_table (
	id varchar(50) NULL,
	param_key varchar(50) NULL,
	param_value varchar(50) NULL,
	category varchar(50) NULL,
	subcategory varchar(50) NULL,
	description varchar(100) NULL,
	agreement_id varchar NULL,
	authorizer_id varchar NULL
);


-- public.ibos_error_master definition

-- Drop table

-- DROP TABLE public.ibos_error_master;

CREATE TABLE public.ibos_error_master (
	error_code varchar(20) NULL,
	error_message varchar(200) NULL,
	error_description varchar(200) NULL,
	error_log_date timestamptz NULL,
	interface_type varchar(30) NULL,
	interface_name varchar(100) NULL
);


-- public.ibos_init_params definition

-- Drop table

-- DROP TABLE public.ibos_init_params;

CREATE TABLE public.ibos_init_params (
	id varchar(50) NULL,
	authorizer_id varchar(50) NULL,
	agreement_id varchar(50) NULL,
	acc_key varchar(2400) NULL,
	refresh_key varchar(2400) NULL,
	creation_date timestamptz(3) NULL,
	description varchar(100) NULL
);


-- public.ibos_payment_init definition

-- Drop table

-- DROP TABLE public.ibos_payment_init;

CREATE TABLE public.ibos_payment_init (
	end_to_end_iden varchar(200) NULL,
	instructed_amount numeric NULL,
	instructed_currency varchar(5) NULL,
	debtoraccount varchar(50) NULL,
	creditoraccount varchar(50) NULL,
	creditorname varchar(200) NULL,
	remittance_info varchar(250) NULL,
	transaction_status varchar(50) NULL,
	payment_id_ig varchar(50) NULL,
	payment_id_ahb varchar(50) NULL,
	mssg_iden varchar(50) NULL,
	creation_datetime timestamptz NULL,
	num_txns numeric NULL,
	payment_info_iden varchar(50) NULL,
	payment_method varchar(50) NULL,
	requested_exec_date timestamptz NULL,
	bank_code varchar(5) NULL,
	account_iban varchar(50) NULL,
	orgnl_mssg_iden varchar(50) NULL,
	orgnl_mssg_iden_name varchar(50) NULL,
	orgnl_num_txns numeric NULL,
	orgnl_payment_id varchar(50) NULL,
	template_id varchar(50) NULL,
	authorizer_id varchar(50) NULL,
	acc_type varchar(50) NULL,
	http_code numeric NULL,
	hb_bank_code varchar NULL,
	ahb_txn_status varchar NULL
);