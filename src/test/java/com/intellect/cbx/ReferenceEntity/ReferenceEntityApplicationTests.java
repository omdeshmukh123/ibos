package com.intellect.cbx.ReferenceEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
class ReferenceEntityApplicationTests {

	@Test
	void contextLoads() throws TransformerException, ParserConfigurationException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.newDocument();
		
		Element root = document.createElement("CstmrPmtStsRpt");
		document.appendChild(root);
		Element OrgnlPmtInfAndSts = document.createElement("OrgnlPmtInfAndSts");
		Element PmtInfId = document.createElement("OrgnlPmtInfId");
		PmtInfId.appendChild(document.createTextNode("24241"));
		OrgnlPmtInfAndSts.appendChild(PmtInfId);
		
		Element txInf = document.createElement("TxInfAndSts");
		Element txSts = document.createElement("TxSts");
		txSts.appendChild(document.createTextNode("acsp"));
		txInf.appendChild(txSts);
		OrgnlPmtInfAndSts.appendChild(txInf);
		root.appendChild(OrgnlPmtInfAndSts);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);

		StreamResult consoleResult = new StreamResult(System.out);
		transformer.transform(source, consoleResult);
	}

}
