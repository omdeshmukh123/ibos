package com.intellect.ibos;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.uuid.Generators;
import com.intellect.ibos.Utils.IBOSConstants;
import com.intellect.ibos.Utils.MTXXXFormatter;
import com.intellect.ibos.Utils.SFTPConnectionUtilForPushingFiles;
import com.intellect.ibos.controller.IbosMainController;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

@Component
@EnableScheduling
@EnableAsync
public class MTXXXSenderScheduler {

	private static final Logger log = LoggerFactory.getLogger(MTXXXSenderScheduler.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Value("${IBOS_TO_SANTANDER}")
	private String ibos_to_sntander;
	
	@Value("${DB_url}")
	String url;

	@Value("${DB_username}")
	String userName;

	@Value("${DB_password}")
	String password;

	

	@Value("${TOMCAT_PATH_FAILED_FILES}")
	String localPath;
	
	@Autowired
	IbosMainController controller ;
	
	// //@Scheduled(cron = "0 0 11,14,17 * * *")
	
	@Scheduled(cron = "1 1 11,14,17 ? * *")
	////@Scheduled(cron = "0 * 5-16 * * *")
	////@Scheduled(fixedDelay = 10000)
	public void startProcessing() {
		// log.info("The time is now {}", dateFormat.format(new Date()));
		System.out.println("MTXXX Sender Scheduler The time is now {}" + dateFormat.format(new Date()));
		SFTPConnectionUtilForPushingFiles lConnectionUtil = new SFTPConnectionUtilForPushingFiles();
		//SFTPConnectionUtilForPullingFiles lConnectionUtil = new SFTPConnectionUtilForPullingFiles();
		
		ChannelSftp lsftp = null;
		DBConnectionUtil dbUtil = new DBConnectionUtil();
		Connection conn = null;

		try {
			// connection *************************
			lsftp = lConnectionUtil.getSFTPConnectionforPushingFiles();
			//lsftp = lConnectionUtil.getSFTPConnectionforPullingFiles();
			
			conn = dbUtil.mGetConnection(url, userName, password);
			
			/*
			 * if(lsftp ==null || lsftp.isClosed() || !lsftp.isConnected()) {
			 * System.out.println("Connection is not established sftp so returning ");
			 * return; }
			 */
			//System.out.println(" Entering start processing for sending "+url);
			
			
			if(conn==null ||conn.isClosed() ) {
				System.out.println("DB connection is not established DB so returning ");
				return;
			}

			Map<String, List<String>> map =getAHBBanksWithAccountsforSantander(conn);
			
			MTXXXFormatter formatter = new MTXXXFormatter();
			for(String bank : map.keySet()) {
				
				for(String accountID  : map.get(bank)) {
					
					UUID generatedReqId = Generators.randomBasedGenerator().generate();
					String pXRequestID = generatedReqId.toString();
					ResponseEntity<String> output= controller.getTransactionsFromIntellect("true", new SimpleDateFormat("yyyy-MM-dd").format(new Date()) , new SimpleDateFormat("yyyy-MM-dd").format(new Date()) , "both", accountID, "applcation/json", pXRequestID, new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					String receiverBic = "BSCHDEFFAXXX";
					String senderBIC="";
					if(IBOSConstants.NORDEA_BANK.equals(bank)) senderBIC = "NDEAFIHHAXXX";
						if(IBOSConstants.PBZ_BANK.equals(bank)) senderBIC = "PBZGHR2XAXXX";
							if(IBOSConstants.ISP_BANK.equals(bank)) senderBIC = "BCITITMMAXXX";
									
					String mt942Message  = formatter.formatMT942Message(output.getBody(), senderBIC,receiverBic);
					String fileName = generateFileName("MT942", accountID, "EUR");
					
					try {
					copyMessageToFile(fileName, ibos_to_sntander, mt942Message,lsftp);
					}
					catch(Exception e) {
						System.out.println("Exception occured  942 while copying message to santander "+e);
						copyMessagetoTomcatServer(fileName, mt942Message);
						
					}
					
					 generatedReqId = Generators.randomBasedGenerator().generate();
					 pXRequestID = generatedReqId.toString();
					ResponseEntity<String> output2= controller.getAccountsfromIntellect("true", accountID, "application/json", pXRequestID, new SimpleDateFormat("yyyy-MM-dd").format(new Date()) );
					String mt941Message  = formatter.formatMT941Message(output2.getBody(),senderBIC,receiverBic);
					//generateFileName
					String fileName1 = generateFileName("MT941", accountID, "EUR");
					try {
					copyMessageToFile(fileName1, ibos_to_sntander, mt941Message,lsftp);
					}
					catch(Exception e ) {
						System.out.println("Exception occured 941 while copying message to santander "+e);
						copyMessagetoTomcatServer(fileName, mt941Message);
					}
					
					
				}
			}
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			//if( lsftp != null) {
				lConnectionUtil.closeConnection();
				//}
			dbUtil.mCloseResources(null, conn);
		}
	}


	private void copyMessageToFile(String fileName, String outgoingFilePathonSFTP, String content,ChannelSftp lSftp)
			throws IOException, SftpException {

		
		System.out.println("filePath::::" + outgoingFilePathonSFTP + " fileName:" + fileName);

		// String fileName = responseDataMap.get("lFileCreationParam");
		// String content = responseDataMap.get(fileName);
		lSftp.cd(outgoingFilePathonSFTP);
		InputStream stream = new ByteArrayInputStream((content.getBytes()));

		lSftp.put(stream, outgoingFilePathonSFTP + fileName);

		stream.close();


	}
	
	
	private void copyMessagetoTomcatServer(String fileName, String content) throws IOException {
		
		    BufferedWriter writer = new BufferedWriter(new FileWriter(localPath+fileName));
		    writer.write(content);
		    writer.close();		    
	}
	
	private Map<String, List<String>> getAHBBanksWithAccountsforSantander(Connection conn) throws Exception {

		PreparedStatement pst = null;
		DBConnectionUtil dbUtil = null;
		String query = "select subcategory ,param_value from ibos_config_param_table where param_key = 'ACCOUNT_ID' and category = 'BANK' and subcategory in(select param_value from ibos_config_param_table where param_key = 'AHB' and category = 'HB' and subcategory ='SAN')";
		ResultSet rs = null;
		Map<String, List<String>> map = new HashMap<>();

		try {
			dbUtil = new DBConnectionUtil();
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				String bank = rs.getString(1);
				String accountID = rs.getString(2);
				if (map.containsKey(bank)) {

					List tempList = map.get(bank);

					if (tempList == null) {
						tempList = new ArrayList<>();
						tempList.add(accountID);
					} else {
						tempList.add(accountID);
					}

					map.put(bank, tempList);

				} else {
					List tempList = new ArrayList<>();
					tempList.add(accountID);
					
					map.put(bank, tempList);

				}
			}

			System.out.println(" map of configurations: " + map);

		} catch (Exception e) {

			e.printStackTrace();
			System.out.println(e.getLocalizedMessage());
			throw e;

		} finally {
			dbUtil.mCloseResources(pst, null);
		}
		return map;
	}
	
	private long num = 1000;
	private String generateFileName(String fileType, String accountIBan, String Currency) {
		num++;
		if(num==9999)num = 1000;
		//<IBAN>.<Currency>.<MTxxx>.<YYYYMMDD>.<NNNN>.txt where xxx can be 940 or 942 and NNNN can be a sequence number as there can be many 942s for the same day

		String temp = accountIBan+"."+Currency+"."+fileType+"."+new SimpleDateFormat("YYYYMMdd").format(new Date())+"."+num+".txt";
		System.out.println(" File Name :"+temp);
		return temp;
	}
}
