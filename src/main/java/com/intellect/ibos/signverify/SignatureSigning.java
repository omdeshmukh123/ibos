package com.intellect.ibos.signverify;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.tomitribe.auth.signatures.Signature;
import org.tomitribe.auth.signatures.Signer;

import com.intellect.ibos.Utils.IBOSConstants;

public class SignatureSigning {

	public void verifysign(String httpPayload, HttpServletRequest request) throws Exception {
		System.out.println("------verifySign-----");
		// Digest and Signature only for PBZ
		final String xReqIdHB = request.getHeader("x-request-id");
		final String digestHB = request.getHeader("digest");
		final String signatureHB = request.getHeader("signature");
		final String tppSignCertificateHB = request.getHeader("tpp-signature-certificate"); // Obtain the Senders(HB) TPP Signature
		String method = request.getMethod();
		String uri = request.getRequestURI();
		
		/*
		 * byte[] a= Files.readAllBytes(Paths.get(Utility.PAYLOAD_FILE)); String
		 * filestring = new String(Files.readAllBytes(Paths.get(Utility.PAYLOAD_FILE)),
		 * "UTF-8");
		 */

		httpPayload = httpPayload.replaceAll("\\r", "");

		/*
        String digestee = new String(Base64.getEncoder().encode(DigestUtils.sha256(httpPayload.getBytes())), "UTF-8");
        String digestff = new String(Base64.getEncoder().encode(DigestUtils.sha256(httpPayload.getBytes())));
        String digestgg = new String(Base64.getEncoder().encode(DigestUtils.sha256(httpPayload)));
        String digesthh = new String(Base64.getEncoder().encode(DigestUtils.sha256(httpPayload)), "UTF-8");
        String digestii = new String(Base64.getEncoder().encode(DigestUtils.sha256(httpPayload.getBytes("UTF-8"))));

		System.out.println("\n a:"+a+
			    "\nfilestring:\n"+filestring+
			    "\nhttpPayload\n"+httpPayload+
				"\ndigestee::::"+digestee+
				"\ndigestff::::"+digestff+
				"\ndigestgg::::"+digestgg+
				"\ndigesthh::::"+digesthh+
				"\ndigestii::::"+digestii+
				"\ndigesthh::::"+digesthh);  
		*/
		
		String computedDigest =generateDigest(httpPayload);

		if (isNullOrEmpty(digestHB) || isNullOrEmpty(signatureHB) || isNullOrEmpty(tppSignCertificateHB) || isNullOrEmpty(xReqIdHB)) {
			//do nothing
		} else {
			System.out.println("\n+++++httpPayload++++\n" + httpPayload
					+ "\n+++++method++++\n" + method + "   uri:" + uri + "\n++++digestHB++++" + digestHB
					+ "\n ++++++computedDigest++++++[" + computedDigest);
			if(computedDigest.equals(digestHB)) 
			{
				Map<String, String> requestHeaders = new LinkedHashMap<>();
				requestHeaders.put("x-request-id", xReqIdHB);
				requestHeaders.put("digest", digestHB);
				//requestHeaders.put("tpp-signature-certificate", formatCertificate(tppSignCertificateHB));
				
				// Cannot recompute the Signature as you need the Private Key
				String signatureIDAL = generateSignature(requestHeaders, method, uri);
				/*
				 * if (!signIDAL.getKeyId().equals(signHB.getKeyId())) throw new
				 * Exception("Invalid KeyId"); if
				 * (!signIDAL.getAlgorithm().equals(signHB.getAlgorithm())) throw new
				 * Exception("Invalid Algorithm"); if
				 * (!signIDAL.getHeaders().equals(signHB.getHeaders())) throw new
				 * Exception("Invalid Headers"); if
				 * (!signIDAL.getSignature().equals(signHB.getSignature())) throw new
				 * Exception("Invalid Signature");
				 */
				
				// Verification
				SignatureVerifying ver = new SignatureVerifying();
				if (!ver.verify(signatureHB, formatCertificate(tppSignCertificateHB).toString(), requestHeaders,
						method, uri)) {
					System.out.println("--------Invalid Signature--------");
					throw new Exception("Invalid Signature");// checked exception
				} else {
					System.out.println("--------Valid Signature--------");
				}
			} else {
				System.out.println("------------Invalid Digest---------");
				throw new Exception("Invalid Digest");// checked exception
			}
		}
	}

    public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty() && str.length()>0)
            return false;
        return true;
    }
    
	public String formatCertificate(String certificate) {
		return certificate.replaceAll("\n", "").replaceAll("\r", "").replace("-----BEGIN CERTIFICATE-----", "")
				.replace("-----END CERTIFICATE-----", "");
	}

	public String generateDigest(String payload) {
		try {
			byte[] digest = MessageDigest.getInstance("SHA-256").digest(payload.getBytes(StandardCharsets.UTF_8));
			return "SHA-256=" + new String(Base64.getEncoder().encode(digest));
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException("Wrong algorithm", e);
		}
	}
	
	public String generateSignature(Map<String, String> requestHeaders, String method, String uri) throws Exception {
		SignatureVerifying ver = new SignatureVerifying();
		
		File f = new File(IBOSConstants.RECEIVER_KEYSTORE);
		FileInputStream fis = new FileInputStream(f);
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
		X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(fis);
		// Obtain the TPP Signature Serial Number etc
		String keyId = ver.getKeyIdFromCertificate(certificate);
		
		
		String algorithm = "rsa-sha256";
		Signature signature = new Signature(keyId, algorithm, null, requestHeaders.keySet().toArray(new String[0]));
		PrivateKey privateKey = Utility.getPrivateKey(); // IDAL Private Key
		Signer signer = new Signer(privateKey, signature);

		Signature signed = signer.sign(method, uri, requestHeaders);
		System.out.println(keyId + "\n********signed*******\n" + signed);

		return signed.toString();
	}
}