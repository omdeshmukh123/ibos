/****************************************************************************/
/* Copyright �  2015 Intellect Design Area Ltd. All rights reserved      	*/
/*                                                                      	*/
/****************************************************************************/
/*  Application  : Intellect Payment Engine 								*/
/*                            												*/
/*  Module Name  : Payment Transaction Process                				*/
/*  File Name    : PshiRestSerClient.java	       				   			*/
/*                                                                      	*/
/*  Description  : This class acts as client for Internal Framework services*/
/* 				 															*/
/*                 	                                                		*/
/*  Author       : Samuel Pothuraju	       									*/
/****************************************************************************/
/* Version Control Block                                              		*/
/* ~~~~~~~ ~~~~~~~ ~~~~~                                              		*/
/*                                                                    		*/
/*    Date    	Version    Author          	Ref.            Description 	*/
/*    ====    	=======    ======          ====             ===========
 /*********************************************************************************

 23-April-2019    1.0     Samuel Pothuraju         			Initial Version
 03-Decem-2019    1.2     Samuel Pothuraju         			Comment unnecessary Logging
 /*********************************************************************************/

package com.intellect.ibos.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.uuid.Generators;
import com.intellect.ibos.MTXXXReceiverScheduler;
import com.intellect.ibos.signverify.SignatureSigning;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;


@Repository
public class PostFrameworkClient {
	
	/**
	 * Instantiating logger for logging purpose
	 */
	private static final Logger logger = LoggerFactory.getLogger(PostFrameworkClient.class);

	private static final Logger mPtLog = LoggerFactory.getLogger(MTXXXReceiverScheduler.class);
	//private static boolean isDebugEnabled; //1.1
	private static ClientConfig config = null;
	private static Client client = null;

	static
	{
		
		//isDebugEnabled = mPtLog.isDebugEnabled(); //1.1
		long starttime = Calendar.getInstance().getTimeInMillis();
		config = new DefaultClientConfig();
		client = Client.create(config);

	}

	public String sendRequest(String reqJson, String baseURI, String path,String accountHoldingBank,HttpServletRequest request) throws Exception{

		String resJson = null;
		WebResource rcResource = null;
		ClientResponse response = null;
		Map<String, String> requestHeaders = null;

		if (mPtLog.isDebugEnabled()) {
			mPtLog.debug("Entering sendRequest, message " + reqJson);
		}
		//try {
			if (mPtLog.isDebugEnabled())
				mPtLog.debug("baseURI : " + baseURI + "  path: " + path);
			
			if (logger.isInfoEnabled()) {
				logger.info("path------->"+ path);
			}
			rcResource = client.resource(getBaseURI(baseURI)).path(path);
			UUID generatedReqId = Generators.randomBasedGenerator().generate();
			String reqId = generatedReqId.toString();
			Date date = java.util.Calendar.getInstance().getTime();
			final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			rcResource.header("Date", sdf.format(date));
			rcResource.header("x-request-id", reqId);
			rcResource.header("Content-Type", "application/json");
		    
			// Compute & send IDALs Digest/Signature to PBZ.
			if(accountHoldingBank.equals(IBOSConstants.PBZ_BANK)) {
				SignatureSigning sgn = new SignatureSigning();			    
			    String computedDigest=sgn.generateDigest(reqJson);
			    
			    BufferedReader reader = new BufferedReader(new FileReader(IBOSConstants.RECEIVER_KEYSTORE));
				String cert = reader.lines().collect( Collectors.joining( System.lineSeparator() ) );
				
				requestHeaders = new LinkedHashMap<>();
		        requestHeaders.put("x-request-id",reqId);  
		        requestHeaders.put("digest", computedDigest);
		        requestHeaders.put("tpp-signature-certificate", sgn.formatCertificate(cert));
		        
		    	if (logger.isInfoEnabled()) {
					logger.info("Request Header of PBZ Bank------->x-request-id<------->"+ reqId);
				}
		    	
		    	if (logger.isInfoEnabled()) {
					logger.info("Request Header of PBZ Bank------->digest<------->"+ computedDigest);
				}
		    	
		    	if (logger.isInfoEnabled()) {
					logger.info("Request Header of PBZ Bank------->tpp-signature-certificate<------->"+ sgn.formatCertificate(cert));
				}
		    	
		    	System.out.println("path------->"+ path);
		    	
				String signatureIDAL = sgn.generateSignature(requestHeaders, "POST", path);
				System.out.println("sendRequest.requestHeaders is ------->"+ requestHeaders);
				
				if (logger.isInfoEnabled()) {
					logger.info("Request Header of PBZ Bank------->signature<------->"+ signatureIDAL);
				}
				requestHeaders.put("signature",signatureIDAL);
			}
			System.out.println("===rcResource.header:"+rcResource.toString());

			if(accountHoldingBank.equals(IBOSConstants.ISP_BANK)) {
				System.out.println("===ISP_BANK-----------r:");
				response = postClientResponseISP(rcResource, reqJson);
			} else {
				System.out.println("PBZ BANK-----------");
				 response = postClientResponse(rcResource, reqJson, requestHeaders);
			}
			
			
			if (response.getStatus() == HttpURLConnection.HTTP_OK || 
					response.getStatus() == HttpURLConnection.HTTP_ACCEPTED ||
						response.getStatus() == HttpURLConnection.HTTP_CREATED) {
					resJson = getResponse(response);
					System.out.println(response.getStatus() +"Entering resJson, message " + resJson);
			}else {
				System.out.println(response.getStatus() +"PBZ Error Response message");
			}
				/*
				 * if (response.getStatus() != HttpURLConnection.HTTP_OK) {
				 * 
				 * } else { resJson = getResponse(response); }
				 */
				/*
				 * } catch (Exception e) { mPtLog.error("sendRequest exception Is ::", e); }
				 * finally { rcResource = null; }
				 */
		return resJson;
	}


	public String sendNordRequest(String reqJson, String baseURI, String path,String accountHoldingBank,HttpServletRequest request,String accTkn) throws Exception {
		String resJson = null;
		WebResource rcResource = null;
		ClientResponse response = null;
		if (mPtLog.isDebugEnabled()) {
			mPtLog.debug("Entering sendRequest, message " + reqJson);
		}
		//try {
			if (mPtLog.isDebugEnabled())
				mPtLog.debug("baseURI : " + baseURI + "  path: " + path);
			rcResource = client.resource(getBaseURI(baseURI)).path(path);
			System.out.println(reqJson+"\nbaseURI : " + baseURI + "  path: " + path+"===Entering sendRequest, message " + rcResource);

			UUID generatedReqId = Generators.randomBasedGenerator().generate();
			String reqId = generatedReqId.toString();
			Date date = java.util.Calendar.getInstance().getTime();
			final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			rcResource.header("Date", sdf.format(date));
			rcResource.header("X-Request-ID", reqId);
			rcResource.header("Content-Type", "application/json");
			
			response = postClientResponseforNordea(rcResource, reqJson, accTkn);
			if (response.getStatus() == HttpURLConnection.HTTP_OK || 
				response.getStatus() == HttpURLConnection.HTTP_ACCEPTED ||
					response.getStatus() == HttpURLConnection.HTTP_CREATED) {
				resJson = getResponse(response);
				System.out.println(response.getStatus() +"Entering resJson, message " + resJson);
			} 
		/*} catch (Exception e) {
			mPtLog.error("sendRequest exception Is ::", e);
		} finally {
			rcResource = null;
		}*/
		return resJson;
	}
	
	
	/**
	* Returns client response.
	* e.g :
	* GET https://dev05.researchcentral.cibcwm.com/LdapServices/service/mailservice/postMailDetails
	* returned a response status of 200 OK
	 * @param resp 
	 * @param request 
	*
	* @param service
	* @return
	*/
	private static ClientResponse postClientResponseforNordea(WebResource resource, String reqJson, String accTkn) throws Exception {
		if(mPtLog.isDebugEnabled()) {//1.1
			mPtLog.debug("Entering inside postClientResponse method "+reqJson);
		}//1.1
  		  final Date currentTime = new Date();
	  	  final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
	  	  sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
	  	  return resource.header("Content-Type", "application/json")
				.header("Authorization", "Bearer "+ accTkn)
				.header("X-IBM-Client-Id", "4ab36164-f93a-45fa-882b-69f100fdead2")
				.header("X-IBM-Client-Secret", "A6xI4hP5oT0lW7iU7vI0dF6vI3iJ4lX6yR4jE8wN0sG4eH6wF3")
				.header("Signature", "SKIP_SIGNATURE_VALIDATION_FOR_SANDBOX")
				.header("X-Nordea-Originating-Date", sdf.format(currentTime))
				.header("X-Nordea-Originating-Host", "api.nordeaopenbanking.com").type(MediaType.APPLICATION_JSON).post(ClientResponse.class, reqJson);
	}
	
	/**
	* Returns client response.
	* e.g :
	* GET https://dev05.researchcentral.cibcwm.com/LdapServices/service/mailservice/postMailDetails
	* returned a response status of 200 OK
	 * @param resp 
	 * @param request 
	*
	* @param service
	* @return
	*/
	private static ClientResponse postClientResponse(WebResource resource, String reqJson, Map requestHeaders) throws Exception {
		if(mPtLog.isDebugEnabled()) {//1.1
			mPtLog.debug("Entering inside postClientResponse method ");
		}//1.1
		System.out.println("=postClientResponse.header:"+resource.toString());

		if(requestHeaders!=null) {
			System.out.println("=requestHeaders.header:"+requestHeaders.toString());
			 return resource.header("Content-Type", "application/json")
						.header("x-request-id", requestHeaders.get("x-request-id"))
						.header("digest", requestHeaders.get("digest"))
						.header("tpp-signature-certificate", requestHeaders.get("tpp-signature-certificate"))
						.header("signature", requestHeaders.get("signature")).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, reqJson);
		} else {
			return resource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, reqJson);
		}
	}
	
	private static ClientResponse postClientResponseISP(WebResource resource, String reqJson) throws Exception {
			System.out.println("Entering inside getClientISPResponse method "+resource);
	        ISPauthorization authobj=new ISPauthorization();
			String bearerToken=authobj.getClientCredentials();
	        return authobj.performPOST(resource.toString(), bearerToken, reqJson);
	}

	/**
	* Returns the response as Application JSON
	* e.g : {"interfaceName":"emailService","errorCode":0,"errorMessage":"Success"}
	*
	* @param service
	* @return
	*/
	private String getResponse(ClientResponse response) throws Exception {
		if(mPtLog.isDebugEnabled()) {//1.1
			mPtLog.debug("Entering inside getResponse method ");
		}//1.1
		return response.getEntity(String.class);
	}

	private URI getBaseURI(String baseURI) {
		if(mPtLog.isDebugEnabled()) {//1.1
			mPtLog.debug("Entering inside getBaseURI method ");
		}//1.1
	    return UriBuilder.fromUri(baseURI).build();
	}

}
