/****************************************************************************/
/* Copyright �  2015 Intellect Design Area Ltd. All rights reserved      	*/
/*                                                                      	*/
/****************************************************************************/
/*  Application  : Intellect Payment Engine 								*/
/*                            												*/
/*  Module Name  : Payment Transaction Process                				*/
/*  File Name    : PshiRestSerClient.java	       				   			*/
/*                                                                      	*/
/*  Description  : This class acts as client for Internal Framework services*/
/* 				 															*/
/*                 	                                                		*/
/*  Author       : Samuel Pothuraju	       									*/
/****************************************************************************/
/* Version Control Block                                              		*/
/* ~~~~~~~ ~~~~~~~ ~~~~~                                              		*/
/*                                                                    		*/
/*    Date    	Version    Author          	Ref.            Description 	*/
/*    ====    	=======    ======          ====             ===========
 /*********************************************************************************

 23-April-2019    1.0     Samuel Pothuraju         			Initial Version
 03-Decem-2019    1.2     Samuel Pothuraju         			Comment unnecessary Logging
 /*********************************************************************************/

package com.intellect.ibos.Utils;

import java.net.HttpURLConnection;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.uuid.Generators;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class GetFrameworkClient {
	
	@Value("${client_id}")
	private String clientid;

	@Value("${client_sec}")
	private String clientsec;

	// private static boolean isDebugEnabled; //1.1
	private static ClientConfig config = null;
	private static Client client = null;

	static {

		// isDebugEnabled = mPtLog.isDebugEnabled(); //1.1
		long starttime = Calendar.getInstance().getTimeInMillis();
		config = new DefaultClientConfig();
		client = Client.create(config);

	}

	public String sendRequesForIntessaPBZGetAccounts(String baseURI, String path, String pWithBalanceValue, String accountHoldingBank) {

		String resJson = null;
		WebResource rcResource = null;

		System.out.println("Entering  Intessa PBZ sendRequest, message ");

		try {
			// long starttime1 = Calendar.getInstance().getTimeInMillis();

			// mPtLog.error(" ***** "+Thread.currentThread().getName() + " : " +
			// (Calendar.getInstance().getTimeInMillis() - starttime1) + " : FrameworkClient
			// resource ");

			// if(mPtLog.isDebugEnabled())System.out.println("rcResource :
			// "+rcResource);//1.1
			// long starttime2 = Calendar.getInstance().getTimeInMillis();
			
			rcResource = client.resource(getBaseURI(baseURI)).path(path).queryParam("withBalance", pWithBalanceValue);
			System.out.println("Routed sendRequesForIntessaPBZGetAccounts Path-------------------" + rcResource);

			ClientResponse response =null;
			if(IBOSConstants.ISP_BANK.equals(accountHoldingBank)) {
				System.out.println("ISP=");
				 response = getClientISPResponse(rcResource);
			} else {
				 response = getClientResponse(rcResource);
			}
			
			// RJDEC22  ClientResponse response = getClientResponse(rcResource);
			
			// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
			// (Calendar.getInstance().getTimeInMillis() - starttime2) + " : FrameworkClient
			// getClientResponse ");

			// if(mPtLog.isDebugEnabled())System.out.println("response : "+response);//1.1

			if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			} else {

				// if(mPtLog.isDebugEnabled())System.out.println("Client Response " +
				// response.toString());//1.1

				// long starttime3 = Calendar.getInstance().getTimeInMillis();
				resJson = getResponse(response);
				// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
				// (Calendar.getInstance().getTimeInMillis() - starttime3) + " : FrameworkClient
				// getResponse ");

				// if(mPtLog.isDebugEnabled())System.out.println("Response message " +
				// resJson);//1.1

			}
		} catch (Exception e) {
			System.out.println("sendRequest exception Is ::" + e);
		} finally {
			/*
			 * config = null; client = null;
			 */
			rcResource = null;

		}
		return resJson;
	}

	public String sendRequesForNordeaGetAccounts(String baseURI, String path, String accTkn) {

		String resJson = null;
		WebResource rcResource = null;

		System.out.println("Entering  nordea sendRequest, message ");

		try {

			System.out.println("baseURI : " + baseURI + "  path: " + path);// 1.1

			// long starttime1 = Calendar.getInstance().getTimeInMillis();
			rcResource = client.resource(getBaseURI(baseURI)).path(path);
			//mPtLog.error(" ***** "+Thread.currentThread().getName() + " : " + (Calendar.getInstance().getTimeInMillis() - starttime1) + " : FrameworkClient resource ");
			
			UUID generatedReqId = Generators.randomBasedGenerator().generate();
			String reqId = generatedReqId.toString();
			rcResource.setProperty("X-Request-ID", reqId);
			rcResource.setProperty("Content-Type", "application/json");
			
			//if(mPtLog.isDebugEnabled())System.out.println("rcResource : "+rcResource);//1.1
			//long starttime2 = Calendar.getInstance().getTimeInMillis();
			
			ClientResponse response = getClientNorResponse(rcResource, accTkn);
			// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
			// (Calendar.getInstance().getTimeInMillis() - starttime2) + " : FrameworkClient
			// getClientResponse ");
			// System.out.println(" ***** "+response.getEntity(String.class));
			// if(mPtLog.isDebugEnabled())System.out.println("response : "+response);//1.1

			if (response.getStatus() != HttpURLConnection.HTTP_OK) {
				System.out.println(" response.getStatus() " + response.getStatus());
			} else {

				// if(mPtLog.isDebugEnabled())System.out.println("Client Response " +
				// response.toString());//1.1
				System.out.println(" pad response.getStatus() " + response.getStatus());
				// long starttime3 = Calendar.getInstance().getTimeInMillis();
				resJson = getResponse(response);
				// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
				// (Calendar.getInstance().getTimeInMillis() - starttime3) + " : FrameworkClient
				// getResponse ");
				System.out.println("resJson : " + resJson);
				// if(mPtLog.isDebugEnabled())System.out.println("Response message " +
				// resJson);//1.1

			}
		} catch (Exception e) {
			System.out.println("sendRequest exception Is ::" + e);
		} finally {
			/*
			 * config = null; client = null;
			 */
			rcResource = null;

		}

		return resJson;
	}
	
	public JSONObject sendRequesForIntessaPBZGetTxn( String baseURI, String path,Map<String, String> queryParameters, String accountHoldingBank) {
		
		String resJson = null;
		WebResource rcResource = null;

		System.out.println("Entering  Intessa PBZ sendRequest, message ");

		try {

			System.out.println("baseURI : " + baseURI + "  path: " + path);// 1.1

			if(queryParameters.get("withBalance")!=null) {
				rcResource = client.resource(getBaseURI(baseURI)).path(path)
						.queryParam("dateFrom", queryParameters.get("dateFrom"))
						.queryParam("dateTo", queryParameters.get("dateTo"))
						.queryParam("bookingStatus", queryParameters.get("bookingStatus"))
						.queryParam("withBalance", queryParameters.get("withBalance"));	
			} else {
				rcResource = client.resource(getBaseURI(baseURI)).path(path)
						.queryParam("dateFrom", queryParameters.get("dateFrom"))
						.queryParam("dateTo", queryParameters.get("dateTo"))
						.queryParam("bookingStatus", queryParameters.get("bookingStatus"));
			}
			// mPtLog.error(" ***** "+Thread.currentThread().getName() + " : " +
			// (Calendar.getInstance().getTimeInMillis() - starttime1) + " : FrameworkClient
			// resource ");

			// if(mPtLog.isDebugEnabled())System.out.println("rcResource :
			// "+rcResource);//1.1
			// long starttime2 = Calendar.getInstance().getTimeInMillis();

			System.out.println("Routed sendRequesForIntessaPBZGetTxn Path------------------ : " + rcResource);
			
			
			ClientResponse response =null;
			if(IBOSConstants.ISP_BANK.equals(accountHoldingBank)) {
				System.out.println("ISP>>");
				 response = getClientISPResponse(rcResource);
			} else {
				 response = getClientResponse(rcResource);
			}
			
			
			//ClientResponse response = getClientResponse(rcResource);
			// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
			// (Calendar.getInstance().getTimeInMillis() - starttime2) + " : FrameworkClient
			// getClientResponse ");

			// if(mPtLog.isDebugEnabled())System.out.println("response : "+response);//1.1

			if(response!=null) {
				if (response.getStatus() != HttpURLConnection.HTTP_OK) {
					System.out.println(response+" (((IF))) "+response.getStatus());
				} else {
					System.out.println(" (((Else))) ");
	
					// if(mPtLog.isDebugEnabled())System.out.println("Client Response " +
					// response.toString());//1.1
	
					// long starttime3 = Calendar.getInstance().getTimeInMillis();
					resJson = getResponse(response);
					// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
					// (Calendar.getInstance().getTimeInMillis() - starttime3) + " : FrameworkClient
					// getResponse ");
	
					// if(mPtLog.isDebugEnabled())System.out.println("Response message " +
					// resJson);//1.1
	
				}
			}
		} catch (Exception e) {
			System.out.println("sendRequest exception Is ::" + e);
		} finally {
			/*
			 * config = null; client = null;
			 */
			rcResource = null;

        }
		if(resJson!=null)
			return new JSONObject(resJson);
		else return null;
	}
	
	public JSONObject sendRequesForNordeaGetTxn( String baseURI, String path, Map<String,String> queryParameters, String accTkn) {
		
		String resJson = null;
		WebResource rcResource = null;

		System.out.println("Entering  nordea sendRequest, message ");

		try {

			System.out.println("baseURI : " + baseURI + "  path: " + path);// 1.1

			// long starttime1 = Calendar.getInstance().getTimeInMillis();
			rcResource = client.resource(getBaseURI(baseURI)).path(path)
					.queryParam("dateFrom", queryParameters.get("dateFrom"))
					.queryParam("dateTo", queryParameters.get("dateTo"))
					;
			//mPtLog.error(" ***** "+Thread.currentThread().getName() + " : " + (Calendar.getInstance().getTimeInMillis() - starttime1) + " : FrameworkClient resource ");
			
			UUID generatedReqId = Generators.randomBasedGenerator().generate();
			String reqId = generatedReqId.toString();
			rcResource.setProperty("X-Request-ID", reqId);
			rcResource.setProperty("Content-Type", "application/json");
			
			//if(mPtLog.isDebugEnabled())System.out.println("rcResource : "+rcResource);//1.1
			//long starttime2 = Calendar.getInstance().getTimeInMillis();
			
			ClientResponse response = getClientNorResponse(rcResource, accTkn);
			// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
			// (Calendar.getInstance().getTimeInMillis() - starttime2) + " : FrameworkClient
			// getClientResponse ");
			// System.out.println(" ***** "+response.getEntity(String.class));
			// if(mPtLog.isDebugEnabled())System.out.println("response : "+response);//1.1

			if (response.getStatus() != HttpURLConnection.HTTP_OK) {
				System.out.println(" response.getStatus() " + response.getStatus());
			} else {

				// if(mPtLog.isDebugEnabled())System.out.println("Client Response " +
				// response.toString());//1.1
				System.out.println(" pad response.getStatus() " + response.getStatus());
				// long starttime3 = Calendar.getInstance().getTimeInMillis();
				resJson = getResponse(response);
				// System.out.println(" ***** "+Thread.currentThread().getName() + " : " +
				// (Calendar.getInstance().getTimeInMillis() - starttime3) + " : FrameworkClient
				// getResponse ");
				System.out.println("resJson : " + resJson);
				// if(mPtLog.isDebugEnabled())System.out.println("Response message " +
				// resJson);//1.1

			}
		} catch (Exception e) {
			System.out.println("sendRequest exception Is ::" + e);
		} finally {
			/*
			 * config = null; client = null;
			 */
    		rcResource = null;
    		

        }
		
		return new JSONObject(resJson);
	}


	public String sendRequestToNordeaFromISP(String baseURI, String path, String accTkn) {

		String resJson = null;
		WebResource rcResource = null;
		try {
			System.out.println("GET  baseURI : " + baseURI + "  path: " + path);
			rcResource = client.resource(getBaseURI(baseURI)).path(path);
			ClientResponse response = getClientNorResponse(rcResource, accTkn);
			//if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			//} else {
				resJson = getResponse(response);
				System.out.println("sendRequest response Is ::" + resJson);

			//}
		} catch (Exception e) {
			System.out.println("sendRequest exception Is ::" + e);
		} finally {
			rcResource = null;
		}
		return resJson;
	}

	private static ClientResponse getClientNorResponse(WebResource resource, String accTkn) {
		System.out.println("Entering inside getClientNorResponse method "+resource);
		final Date currentTime = new Date();

    	final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
  	    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
  	    return resource.header("Content-Type", "application/json")
			.header("Authorization", "Bearer "+ accTkn)
			.header("X-IBM-Client-Id", "4ab36164-f93a-45fa-882b-69f100fdead2")
			.header("X-IBM-Client-Secret", "A6xI4hP5oT0lW7iU7vI0dF6vI3iJ4lX6yR4jE8wN0sG4eH6wF3")
			.header("Signature", "SKIP_SIGNATURE_VALIDATION_FOR_SANDBOX")
		    .header("X-Nordea-Originating-Date", sdf.format(currentTime))
		    .header("X-Nordea-Originating-Host", "api.nordeaopenbanking.com").type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
	}

	
	public String sendRequestToNordeaFromSan(String baseURI, String path, String accTkn) {

		String resJson = null;
		WebResource rcResource = null;
		try {
			System.out.println("baseURI : " + baseURI + "  path: " + path);
			rcResource = client.resource(getBaseURI(baseURI)).path(path);
			ClientResponse response = getClientNorResponse(rcResource, accTkn);
			if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			} else {
				resJson = getResponse(response);
			}
		} catch (Exception e) {
			System.out.println("sendRequest exception Is ::" + e);
		} finally {
			rcResource = null;
		}
		return resJson;
	}

	
	/**
	 * Returns client response. e.g : GET
	 * https://dev05.researchcentral.cibcwm.com/LdapServices/service/mailservice/postMailDetails
	 * returned a response status of 200 OK
	 *
	 * @param service
	 * @return
	 */
	private static ClientResponse getClientResponse(WebResource resource) {
		System.out.println("Entering inside getClientResponse method ");
		return resource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
	}

	private static ClientResponse getClientISPResponse(WebResource resource) throws Exception {
		System.out.println("Entering inside getClientISPResponse method "+resource);
        ISPauthorization authobj=new ISPauthorization();
		String bearerToken=authobj.getClientCredentials();
        return authobj.performGet(resource.toString(), bearerToken);
	}
	
	/**
	 * Returns the response as Application JSON e.g :
	 * {"interfaceName":"emailService","errorCode":0,"errorMessage":"Success"}
	 *
	 * @param service
	 * @return
	 */
	private String getResponse(ClientResponse response) {
		System.out.println("Entering inside getResponse method ");
		return response.getEntity(String.class);
	}

	private URI getBaseURI(String baseURI) {
		System.out.println("Entering inside getBaseURI method ");
		return UriBuilder.fromUri(baseURI).build();
	}

	public String sendRequestToPBZFromISP(String pbzURI, String pEndpoint) {
		String resJson = null;
		WebResource rcResource = null;
		try {
			System.out.println("baseURI : " + pbzURI + "  path: " + pEndpoint);
			rcResource = client.resource(getBaseURI(pbzURI)).path(pEndpoint);
			ClientResponse response = getClientResponse(rcResource);
			if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			} else {
				resJson = getResponse(response);
			}
		} catch (Exception e) {
			System.out.println("sendRequestToPBZFromISP exception Is ::" + e);
		} finally {
			rcResource = null;
		}
		return resJson;
	}
	
	public String sendRequestToPBZFromSan(String pbzURI, String pEndpoint) {
		String resJson = null;
		WebResource rcResource = null;
		try {
			System.out.println("baseURI : " + pbzURI + "  path: " + pEndpoint);
			rcResource = client.resource(getBaseURI(pbzURI)).path(pEndpoint);
			ClientResponse response = getClientResponse(rcResource);
			if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			} else {
				resJson = getResponse(response);
			}
		} catch (Exception e) {
			System.out.println("sendRequestToPBZFromsSan exception Is ::" + e);
		} finally {
			rcResource = null;
		}
		return resJson;
	}

	public String sendRequestToNordeaFromPbz(String nordeaURI, String pEndpoint, String accTkn) {
		String resJson = null;
		WebResource rcResource = null;
		try {
			System.out.println("baseURI : " + nordeaURI + "  path: " + pEndpoint);
			rcResource = client.resource(getBaseURI(nordeaURI)).path(pEndpoint);
			ClientResponse response = getClientNorResponse(rcResource, accTkn); //rjdec07
			if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			} else {
				resJson = getResponse(response);
			}
		} catch (Exception e) {
			System.out.println("sendRequestToNordeaFromPbz exception Is ::" + e);
		} finally {
			rcResource = null;
		}
		return resJson;
	}

	public String sendRequestToISPFromPBZ(String intessaURI, String pEndpoint) throws Exception {
		String resJson = null;
		WebResource rcResource = null;
		try {
			System.out.println("baseURI : " + intessaURI + "  path: " + pEndpoint);
			rcResource = client.resource(getBaseURI(intessaURI)).path(pEndpoint);
			ClientResponse response = getClientISPResponse(rcResource); // Changes here
			if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			} else {
				resJson = getResponse(response);
			}
		} catch (Exception e) {
			System.out.println("sendRequestToISPFromPBZ exception Is ::" + e);
		} finally {
			rcResource = null;
		}
		return resJson;
	}
	
	public String sendRequestToISPFromSan(String intessaURI, String pEndpoint) {
		String resJson = null;
		WebResource rcResource = null;
		try {
			System.out.println("baseURI : " + intessaURI + "  path: " + pEndpoint);
			rcResource = client.resource(getBaseURI(intessaURI)).path(pEndpoint);
			ClientResponse response = getClientISPResponse(rcResource); // Changes here
			if (response.getStatus() != HttpURLConnection.HTTP_OK) {

			} else {
				resJson = getResponse(response);
			}
		} catch (Exception e) {
			System.out.println("sendRequestToISPFromSan exception Is ::" + e);
		} finally {
			rcResource = null;
		}
		return resJson;
	}

}
