package com.intellect.ibos.Utils;

/*
 * NORDEA Access Token Generator
 */

import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@Repository
public class TokenGenerator {

	private static ClientConfig config = null;
	private static Client client = null;

	static
	{
		config = new DefaultClientConfig();
		client = Client.create(config);
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		
		TokenGenerator obj = new TokenGenerator();
        Map<String,String> ahmp = new HashMap<String,String>(); 
		ahmp.put("70313276", "130348721209");
		ahmp.put("70313514","130348721209");
		ahmp.put("70311198", "130474822427");
		ahmp.put("70312055","130474822427");
		
	    
	    // looping over keys 
        for (String authid : ahmp.keySet())  
        { 
            // search  for value 
            String agreementid = ahmp.get(authid); 
            System.out.println("Key = " + authid + ", Value = " + agreementid); 
            obj.accessTokenNordea(agreementid,authid);
        } 
	}
	
    public HashMap accessTokenNordea(String agreementID, String authorizerID) { 
  	  WebResource resource = null;
  	  ClientResponse response = null;
  	  HashMap hmaps = null;
  	  String resJson=null, access_token=null, refresh_token=null, code=null;
  	try {
      System.out.println(authorizerID+"---------Initiate Authorization------------"+agreementID);
  	  String baseURI="https://api.nordeaopenbanking.com";
  	  String path="/corporate/v2/authorize";
  	  String reqJson="{\"scope\": [\"PAYMENTS_BROADBAND\", \"ACCOUNTS_BROADBAND\"],"
  	  		+ "\"duration\":129600,"
  	  		+ "\"agreement_number\":\""+agreementID+"\"}";
  	  resource = client.resource(getBaseURI(baseURI)).path(path); 
  	  
  	  final Date currentTime = new Date(); 
  	  final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
  	  sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
  	  
  	  System.out.println(reqJson+"*******Req & Resource****"+resource); 
      response= postClientResponse(resource, reqJson, sdf.format(currentTime), "");
  	  System.out.println("response:-"+response); 
  	  resJson = getResponse(response);
	  System.out.println("Status:-"+response.getStatus());

	  JSONObject result = new JSONObject(resJson).getJSONObject("response");
	  String access_id=(String) result.get("access_id");
	  String client_token=(String) result.get("client_token");
      System.out.println(access_id+"-access_id...client_token:-"+client_token);
      
      System.out.println("-------------Confirm Authorization------------");
  	  baseURI="https://api.nordeaopenbanking.com";
  	  path="/corporate/v2/authorize/"+access_id;
  	  resource = client.resource(getBaseURI(baseURI)).path(path); 
  	  System.out.println("resource:-"+resource);
  	  
      reqJson="{\"authorizer_id\":\""+authorizerID+"\"}";
      response= putClientResponse(resource, reqJson, sdf.format(currentTime), client_token);
  	  resJson = getResponse(response);
      System.out.println("resJson:-"+resJson);

      result = new JSONObject(resJson).getJSONObject("response");
	  String status=(String) result.get("status");
      System.out.println(access_id+" ... "+status);
      
      if(!"ACTIVE".equals(status) ) {	  
     	  System.out.println("------------Authorization Status---------");
    	  int i=0;
    	  do {
    	  	  System.out.println(" resource... "+resource);
	    	  response= getClientResponse(resource, sdf.format(currentTime), client_token);
	      	  resJson = getResponse(response);
	          result = new JSONObject(resJson).getJSONObject("response");
	    	  status=(String) result.get("status");
	    	  i++; // to use to restrict the calls
	    	  System.out.println(i+"  Status"+status);
    	  }
    	  while (!"ACTIVE".equals(status) && i<10);
    	  
    	  if("ACTIVE".equals(status)) {
    		  code=(String) result.get("code");
              System.out.println(" Code:-]"+code);
           
        	  System.out.println("----------Exchange Token--------------");
              baseURI="https://api.nordeaopenbanking.com";
          	  path="/corporate/v2/authorize/token";
          	  resource = client.resource(getBaseURI(baseURI)).path(path); 
          	  System.out.println("resource:-]"+resource);
          	  
          	  response=postClientUrlEncodResponse(resource, code, sdf.format(currentTime));    
          	  resJson = getResponse(response);
              result = new JSONObject(resJson).getJSONObject("response");
              access_token=(String) result.get("access_token");
              refresh_token= (String) result.get("refresh_token");

              result = new JSONObject(resJson).getJSONObject("group_header");
              System.out.println("creation_date_time:-]"+result.get("creation_date_time")+" http_code:-"+result.get("http_code"));

              hmaps = new HashMap();
              hmaps.put("access_token", access_token);
              hmaps.put("creation_time", result.get("creation_date_time"));
              System.out.println(access_token+"\n^^^^^^^^^^^^^^^^FINAL^^^^^^^^^^^^^^^^^^^^^^^");
              
    	  }
      }
  	} catch (Exception e) {
  	  e.printStackTrace();
  	}
  	  return hmaps;
  }
    
    private String getResponse(ClientResponse response) {
		return response.getEntity(String.class);
	}
    
	private URI getBaseURI(String baseURI) {
	    return UriBuilder.fromUri(baseURI).build();
	}
	
    private ClientResponse postClientResponse(WebResource resource, String reqJson, String date, String bearer) {	
	  return resource.header("Content-Type", "application/json")
			  .header("Authorization", "Bearer "+bearer)
  	  	  	  .header("X-IBM-Client-Id", "4ab36164-f93a-45fa-882b-69f100fdead2")
  	  	  	  .header("X-IBM-Client-Secret","A6xI4hP5oT0lW7iU7vI0dF6vI3iJ4lX6yR4jE8wN0sG4eH6wF3")
  	  	  	  .header("Signature","SKIP_SIGNATURE_VALIDATION_FOR_SANDBOX")
  	  	  	  .header("X-Nordea-Originating-Date",date )
  	  	  	  .header("X-Nordea-Originating-Host","api.nordeaopenbanking.com").type(MediaType.APPLICATION_JSON).post(ClientResponse.class, reqJson); 
    }

    private ClientResponse putClientResponse(WebResource resource, String reqJson, String date, String bearer) {	
	  return resource.header("Content-Type", "application/json")
			  .header("Authorization", "Bearer "+bearer)
  	  	  	  .header("X-IBM-Client-Id", "4ab36164-f93a-45fa-882b-69f100fdead2")
  	  	  	  .header("X-IBM-Client-Secret","A6xI4hP5oT0lW7iU7vI0dF6vI3iJ4lX6yR4jE8wN0sG4eH6wF3")
  	  	  	  .header("Signature","SKIP_SIGNATURE_VALIDATION_FOR_SANDBOX")
  	  	  	  .header("X-Nordea-Originating-Date",date )
  	  	  	  .header("X-Nordea-Originating-Host","api.nordeaopenbanking.com").type(MediaType.APPLICATION_JSON).put(ClientResponse.class, reqJson); 
    }
    
    private ClientResponse postClientUrlEncodResponse(WebResource resource, String code, String date) {	
    	MultivaluedMapImpl values = new MultivaluedMapImpl();
    	values.add("grant_type", "authorization_code");
    	values.add("code", code);
	    return resource.header("Content-Type", "application/json")
  	  	  	  .header("X-IBM-Client-Id", "4ab36164-f93a-45fa-882b-69f100fdead2")
  	  	  	  .header("X-IBM-Client-Secret","A6xI4hP5oT0lW7iU7vI0dF6vI3iJ4lX6yR4jE8wN0sG4eH6wF3")
  	  	  	  .header("Signature","SKIP_SIGNATURE_VALIDATION_FOR_SANDBOX")
  	  	  	  .header("X-Nordea-Originating-Date",date )
  	  	  	  .header("X-Nordea-Originating-Host","api.nordeaopenbanking.com").type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, values); 
    }
    
    private ClientResponse getClientResponse(WebResource resource, String date, String bearer) {	
	  return resource.header("Content-Type", "application/json")
			  .header("Authorization", "Bearer "+bearer)
  	  	  	  .header("X-IBM-Client-Id", "4ab36164-f93a-45fa-882b-69f100fdead2")
  	  	  	  .header("X-IBM-Client-Secret","A6xI4hP5oT0lW7iU7vI0dF6vI3iJ4lX6yR4jE8wN0sG4eH6wF3")
  	  	  	  .header("Signature","SKIP_SIGNATURE_VALIDATION_FOR_SANDBOX")
  	  	  	  .header("X-Nordea-Originating-Date",date )
  	  	  	  .header("X-Nordea-Originating-Host","api.nordeaopenbanking.com").type(MediaType.APPLICATION_JSON).get(ClientResponse.class); 
    }
}