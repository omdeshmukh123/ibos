
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateFormatter {
	public static String convertToGMTDate(Date date, String format, String timeZone) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
			timeZone = Calendar.getInstance().getTimeZone().getID();
		}
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		return sdf.format(date);
	}

	public static void main(String[] args) {
		// Test formatDateToString method
		Date date = new Date();
		convertToGMTDate(date, "YYYY-MM-dd'T'hh:mm:ss.sss", "GMT");

	}

}