import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.support.CronSequenceGenerator;

import com.intellect.ibos.Utils.IBOSConstants;
import com.intellect.ibos.Utils.MTXXXFormatter;
import com.intellect.ibos.Utils.SFTPConnectionUtilForPullingFiles;
import com.jcraft.jsch.SftpException;

public class Test {

	
	public static void main(String[] args) throws IOException, SftpException, ParseException {
		/*
		 * 
		 * String s
		 * ="{\"transactions\":{\"booked\":[{\"transactionAmount\":{\"amount\":\"1853.99\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-10-15\",\"valueDate\":\"2018-10-17\",\"transactionId\":\"1808232584LV360048\"},{\"transactionAmount\":{\"amount\":\"911.98\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-10-12\",\"valueDate\":\"2018-10-13\",\"transactionId\":\"1808232584LV360063\"},{\"transactionAmount\":{\"amount\":\"1489.67\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-10-11\",\"valueDate\":\"2018-10-12\",\"transactionId\":\"1808232584LV360064\"},{\"transactionAmount\":{\"amount\":\"357.05\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-09-28\",\"valueDate\":\"2018-09-29\",\"transactionId\":\"1808232584LV360117\"},{\"transactionAmount\":{\"amount\":\"16.07\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-09-25\",\"valueDate\":\"2018-09-26\",\"transactionId\":\"1808232584LV360126\"},{\"transactionAmount\":{\"amount\":\"245.67\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-09-19\",\"valueDate\":\"2018-09-20\",\"transactionId\":\"1808232584LV360157\"},{\"transactionAmount\":{\"amount\":\"77.50\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-09-08\",\"valueDate\":\"2018-09-09\",\"transactionId\":\"1808232584LV360196\"},{\"transactionAmount\":{\"amount\":\"460.23\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-08-05\",\"valueDate\":\"2018-08-06\",\"transactionId\":\"1808232584LV360338\"},{\"transactionAmount\":{\"amount\":\"71.69\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-07-30\",\"valueDate\":\"2018-07-31\",\"transactionId\":\"1808232584LV360358\"},{\"transactionAmount\":{\"amount\":\"97.63\"},\"creditorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"bookingDate\":\"2018-07-27\",\"valueDate\":\"2018-07-28\",\"transactionId\":\"1808232584LV360370\"}],\"pending\":[{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-4.08\"},\"bookingDate\":\"2018-10-29\",\"valueDate\":\"2018-10-30\",\"transactionId\":\"1808232584LV360000\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Otto Rekunen\",\"transactionAmount\":{\"amount\":\"-2380.82\"},\"bookingDate\":\"2018-10-14\",\"debtorName\":\"Otto Rekunen\",\"valueDate\":\"2018-10-16\",\"transactionId\":\"1808232584LV360055\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Eini Virtanen\",\"transactionAmount\":{\"amount\":\"-6.87\"},\"bookingDate\":\"2018-10-13\",\"debtorName\":\"Eini Virtanen\",\"valueDate\":\"2018-10-14\",\"transactionId\":\"1808232584LV360059\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Leena Järvinen\",\"transactionAmount\":{\"amount\":\"-21.66\"},\"bookingDate\":\"2018-10-04\",\"debtorName\":\"Leena Järvinen\",\"valueDate\":\"2018-10-06\",\"transactionId\":\"1808232584LV360088\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-14.72\"},\"bookingDate\":\"2018-10-03\",\"valueDate\":\"2018-10-04\",\"transactionId\":\"1808232584LV360093\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Laina Uronen\",\"transactionAmount\":{\"amount\":\"-272.16\"},\"bookingDate\":\"2018-09-20\",\"debtorName\":\"Laina Uronen\",\"valueDate\":\"2018-09-23\",\"transactionId\":\"1808232584LV360152\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Boliden AB\",\"transactionAmount\":{\"amount\":\"-1921.46\"},\"bookingDate\":\"2018-09-20\",\"debtorName\":\"Boliden AB\",\"valueDate\":\"2018-09-21\",\"transactionId\":\"1808232584LV360156\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-2163.14\"},\"bookingDate\":\"2018-09-19\",\"valueDate\":\"2018-09-21\",\"transactionId\":\"1808232584LV360161\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Mari Haanpää\",\"transactionAmount\":{\"amount\":\"-116.90\"},\"bookingDate\":\"2018-09-17\",\"debtorName\":\"Mari Haanpää\",\"valueDate\":\"2018-09-18\",\"transactionId\":\"1808232584LV360166\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-2.04\"},\"bookingDate\":\"2018-09-10\",\"valueDate\":\"2018-09-13\",\"transactionId\":\"1808232584LV360191\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-78.73\"},\"bookingDate\":\"2018-09-08\",\"valueDate\":\"2018-09-09\",\"transactionId\":\"1808232584LV360198\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-6.49\"},\"bookingDate\":\"2018-08-31\",\"valueDate\":\"2018-09-01\",\"transactionId\":\"1808232584LV360230\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Eila Jukola\",\"transactionAmount\":{\"amount\":\"-579.03\"},\"bookingDate\":\"2018-08-30\",\"debtorName\":\"Eila Jukola\",\"valueDate\":\"2018-08-31\",\"transactionId\":\"1808232584LV360235\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Ericsson\",\"transactionAmount\":{\"amount\":\"-1267.81\"},\"bookingDate\":\"2018-08-23\",\"debtorName\":\"Ericsson\",\"valueDate\":\"2018-08-25\",\"transactionId\":\"1808232584LV360261\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-2.33\"},\"bookingDate\":\"2018-08-10\",\"valueDate\":\"2018-08-11\",\"transactionId\":\"1808232584LV360313\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-382.06\"},\"bookingDate\":\"2018-08-07\",\"valueDate\":\"2018-08-08\",\"transactionId\":\"1808232584LV360327\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Boliden AB\",\"transactionAmount\":{\"amount\":\"-1015.60\"},\"bookingDate\":\"2018-07-25\",\"debtorName\":\"Boliden AB\",\"valueDate\":\"2018-07-26\",\"transactionId\":\"1808232584LV360378\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Carlsberg Group\",\"transactionAmount\":{\"amount\":\"-431.92\"},\"bookingDate\":\"2018-07-25\",\"debtorName\":\"Carlsberg Group\",\"valueDate\":\"2018-07-26\",\"transactionId\":\"1808232584LV360379\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"creditorName\":\"Laina Uronen\",\"transactionAmount\":{\"amount\":\"-1457.65\"},\"bookingDate\":\"2018-07-24\",\"debtorName\":\"Laina Uronen\",\"valueDate\":\"2018-07-26\",\"transactionId\":\"1808232584LV360380\"},{\"debtorAccount\":{\"iban\":\"FI5115963000005662-EUR\"},\"transactionAmount\":{\"amount\":\"-14.28\"},\"bookingDate\":\"2018-07-24\",\"valueDate\":\"2018-07-25\",\"transactionId\":\"1808232584LV360381\"}]},\"account\":{\"iban\":\"FI5115963000005662-EUR\"}}"
		 * ;
		 * 
		 * MTXXXFormatter fors = new MTXXXFormatter();
		 * 
		 * String receiverBic = "BSCHDEFFAXXX"; String senderBIC=""; String bank="NOR";
		 * if(IBOSConstants.NORDEA_BANK.equals(bank)) senderBIC = "NDEAFIHHAXXX";
		 * if(IBOSConstants.PBZ_BANK.equals(bank)) senderBIC = "PBZGHR2XAXXX";
		 * if(IBOSConstants.ISP_BANK.equals(bank)) senderBIC = "BCITITMMAXXX";
		 * 
		 * //{1:F01 NDEAFIHHAXXX 0000000000}{2:I942 BSCHDEFFXXXX N} //{1:F01
		 * PBZGHR2XAXXX 1234123456}{2:O942113020 1215 BSCHDEFFAXXX 1234 123456
		 * 2012151130N} System.out.println(fors.formatMT942Message(s, senderBIC,
		 * receiverBic));
		 * 
		 * 
		 * String MT941 =
		 * "{\"balances\":[{\"balanceType\":\"forwardAvailable\",\"lastChangedDateTime\":\"2018-10-29\",\"referenceDate\":\"2020-12-02\",\"balanceAmount\":{\"amount\":\"639638.00\",\"currency\":\"EUR\"}},{\"balanceType\":\"openingBooked\",\"lastChangedDateTime\":\"2018-10-29\",\"referenceDate\":\"2020-12-02\",\"balanceAmount\":{\"amount\":\"639638.00\",\"currency\":\"EUR\"}},{\"balanceType\":\"closingBooked\",\"lastChangedDateTime\":\"2018-10-29\",\"referenceDate\":\"2020-12-02\",\"balanceAmount\":{\"amount\":\"639638.00\",\"currency\":\"EUR\"}}],\"account\":{\"iban\":\"FI5115963000005662-EUR\",\"name\":\"VDB AUTO SUPPLIES\",\"currency\":\"EUR\",\"status\":\"enabled\"}}"
		 * ;
		 * 
		 * System.out.println(fors.formatMT941Message(MT941, senderBIC, receiverBic));
		 * // SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd"); //
		 * SimpleDateFormat sf2 = new SimpleDateFormat("yyMMdd"); // //
		 * System.out.println(sf2.format(sf.parse("2020-12-03"))); // // BigDecimal m =
		 * new BigDecimal(912345); // System.out.println(m.abs());
		 * System.out.println(s); System.out.println(MT941);
		 * 
		 * String s1 = "FI5115963000005662-EasasUR"; if(s1.contains("-")) {
		 * s1.substring(s1.lastIndexOf("-")); }
		 * System.out.println(s1.substring(0,s1.lastIndexOf("-")));
		 * 
		 * 
		 * String TxnId = "abcdefghijklmnop"; try { if(TxnId.length()>16) { TxnId =
		 * TxnId.substring(2, TxnId.length()); } } catch(Exception e ) {
		 * e.printStackTrace(); }
		 * 
		 * System.out.println(TxnId);
		 * 
		 * //System.out.println(Long.parseLong("1853.99"));
		 */
		
		
		  SFTPConnectionUtilForPullingFiles pul = new
		  SFTPConnectionUtilForPullingFiles(); 
		  pul.getSFTPConnectionforPullingFiles();
		  
		  pul.closeConnection();
		
		
//		CronSequenceGenerator c = new CronSequenceGenerator("0/10 0 09-17 ? * *");
//		System.out.println(c.isValidExpression("0 0 11,14,17 ? * *"));
	
	}
		
		
		
	
	
}
